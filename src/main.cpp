#include <iostream>
#include <string>

#include <pcap.h>

#include "Processor.hpp"


int main(int argc, char** argv)
{
	std::string errorBuffer;
	errorBuffer.reserve(PCAP_ERRBUF_SIZE);
	errorBuffer.resize(PCAP_ERRBUF_SIZE);
    if(argc < 3)
    {
        std::cout << "Usage: dumpdigger <db connection string> <pcap file>" << std::endl;
        return 1;
    }
	std::string dumpFile = argv[argc - 1];
    std::string connStr = argv[argc - 2];

	pcap_t* handle = pcap_open_offline(dumpFile.c_str(), &errorBuffer[0]);
	if(handle == nullptr)
	{
		std::cout << "Failed to open dump file: " << dumpFile << std::endl;
		std::cout << "Error: " << errorBuffer << std::endl;
		return 1;
	}

	pcap_pkthdr header;
	const u_char* packet;

    Processor processor(connStr);

	do
	{
        try
        {
            packet = pcap_next(handle, &header);
            if(packet != nullptr)
            {
                auto p = Packet(packet, header.caplen);
                processor.process(p);
            }
        } catch(const std::runtime_error& e)
        {
            std::cout << "Error: " << e.what() << std::endl;
        }
	} while(packet != nullptr);

	pcap_close(handle);

    std::cout << "Done." << std::endl;

	return 0;
}

#ifndef __STREAM_HPP__
#define __STREAM_HPP__

#include "Packet.hpp"

class Stream
{
public:
    Stream(const std::string& from, const std::string& to);
    bool canAccept(const Packet& packet) const;
    void accept(const Packet& packet);

    bool finished() const;
    const std::string& getRequest() const;
    const std::string& getResponse() const;
private:
    std::string m_from;
    std::string m_to;
    std::string m_request;
    std::string m_response;
    bool m_fin;
};

#endif
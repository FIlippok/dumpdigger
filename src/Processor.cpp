#include "Processor.hpp"

#include <iostream>
#include <memory>

#include <libpq-fe.h>

Processor::Processor(const std::string& connectionString):
m_connStr(connectionString)
{
}

void Processor::process(const Packet& packet)
{
    if(packet.hasSyn() && !packet.hasAck()) // Connection initiation
    {
        auto stream = Stream(packet.getFrom(), packet.getTo());
        m_streams.emplace_back(stream);
        stream.accept(packet);
    } else
    {
        for(auto it = m_streams.begin(); it != m_streams.end(); ++it)
        {
            if(it->canAccept(packet))
            {
                it->accept(packet);
                if(it->finished())
                {
                    processStream(*it);
                    m_streams.erase(it);
                    break;
                }
            }
        }
    }
}

std::string Processor::getBody(const std::string& http) const
{
    auto startPos = http.find("\r\n\r\n");
    return http.substr(startPos + 4);
}

Processor::RequestInfo Processor::processRequest(const std::string& request) const
{
    auto postStart = request.find("POST ");
    auto pathEnd = request.find(" ", postStart + 5);

    RequestInfo result;
    result.Path = request.substr(postStart + 5, pathEnd - postStart - 5);
    result.Data = getBody(request);
    return result;
}

Processor::ResponseInfo Processor::processResponse(const std::string& response) const
{
    ResponseInfo result;
    result.Data = getBody(response);
    return result;
}

void Processor::processStream(const Stream& stream)
{
    auto request = processRequest(stream.getRequest());
    auto response = processResponse(stream.getResponse());

    static const std::string QUERY("insert into captured_requests(path, request, response) values($1::text, $2::text, $3::text)");
    const char*insertParams[3];
    insertParams[0] = request.Path.c_str();
    insertParams[1] = request.Data.c_str();
    insertParams[2] = response.Data.c_str();
    std::unique_ptr<PGconn, decltype(&PQfinish)> connection(PQconnectdb(m_connStr.c_str()), PQfinish);
    if (PQstatus(connection.get()) == CONNECTION_BAD)
        throw std::runtime_error("DB connection failed.");
    std::unique_ptr<PGresult, decltype(&PQclear)> result(
            PQexecParams(connection.get(), QUERY.c_str(), 3, nullptr,
                         insertParams, nullptr, nullptr, 0), PQclear);
    if (PQresultStatus(result.get()) != PGRES_COMMAND_OK)
        throw std::runtime_error("DB request failed");

}
#ifndef __PROCESSOR_HPP__
#define __PROCESSOR_HPP__

#include "Stream.hpp"

#include <list>
#include <utility>

class Processor
{
public:
    Processor(const std::string& connectionString);

    void process(const Packet& packet);
private:
    struct RequestInfo
    {
        std::string Data;
        std::string Path;
    };

    struct ResponseInfo
    {
        std::string Data;
    };

    std::string getBody(const std::string& http) const;
    RequestInfo processRequest(const std::string& request) const;
    ResponseInfo processResponse(const std::string& response) const;
    void processStream(const Stream& stream);

    std::string m_connStr;
    std::list<Stream> m_streams;
};

#endif
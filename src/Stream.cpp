#include "Stream.hpp"

Stream::Stream(const std::string& from, const std::string& to):
m_from(from),
m_to(to),
m_fin(false)
{
}

bool Stream::canAccept(const Packet& packet) const
{
    auto from = packet.getFrom();
    auto to = packet.getTo();
    if(from == m_from) return to == m_to;
    if(from == m_to) return to == m_from;
    return false;
}

void Stream::accept(const Packet& packet)
{
    auto from = packet.getFrom();
    auto to = packet.getTo();
    m_fin = packet.hasFin();
    if(from == m_from && to == m_to)
        m_request += packet.getData();
    if(from == m_to && to == m_from)
        m_response += packet.getData();
}

bool Stream::finished() const
{
    return m_fin;
}

const std::string& Stream::getRequest() const
{
    return m_request;
}

const std::string& Stream::getResponse() const
{
    return m_response;
}
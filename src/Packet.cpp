#include "Packet.hpp"

#include <iostream>

#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/if_ether.h>
#include <stdexcept>
#include <arpa/inet.h>


Packet::Packet(const unsigned char* packetData, size_t length):
m_syn(0),
m_ack(0),
m_hasSyn(false),
m_hasAck(false),
m_hasFin(false)
{
    auto packet = const_cast<unsigned char*>(packetData);
    auto packetLength = length;

    if(packetLength < sizeof(ether_header))
        throw std::runtime_error("Too small packet -> no ethernet header");

    packet += sizeof(ether_header);
    packetLength -= sizeof(ether_header);
    if(packetLength < sizeof(ip))
        throw std::runtime_error("Too small packet -> no ip header");

    auto ipHeader = reinterpret_cast<iphdr*>(packet);
    packet += sizeof(iphdr);
    packetLength -= sizeof(iphdr);
    if(ipHeader->protocol != IPPROTO_TCP)
        throw std::runtime_error("Not tcp packet.");

    if(packetLength < sizeof(tcphdr))
        throw std::runtime_error("Too small packet -> no tcp header");

    auto tcpHeader = reinterpret_cast<tcphdr*>(packet);
    auto dataPtr = packet + tcpHeader->doff * sizeof(uint32_t);
    packet += sizeof(tcphdr);
    packetLength -= sizeof(tcphdr);

    m_from = inet_ntoa(*(reinterpret_cast<in_addr*>(&ipHeader->saddr)));
    m_to = inet_ntoa(*(reinterpret_cast<in_addr*>(&ipHeader->daddr)));

    m_from += ":" + std::to_string(htons(tcpHeader->source));
    m_to += ":" + std::to_string(htons(tcpHeader->dest));

    if(packetLength > 0)
    {
        auto diff = dataPtr - packet;
        auto dataLength = packetLength - diff;
        m_data = std::string(reinterpret_cast<const char*>(dataPtr), dataLength);
    }

    if(tcpHeader->syn)
    {
        m_syn = htonl(tcpHeader->seq);
        m_hasSyn = true;
    }

    if(tcpHeader->ack)
    {
        m_ack = htonl(tcpHeader->ack_seq);
        m_hasAck = true;
    }

    if(tcpHeader->fin)
        m_hasFin = true;
}

void Packet::printInfo() const
{
    std::cout << "Packet{" << m_from << " -> " << m_to << "}: ";
    if(m_hasSyn)
        std::cout << "SYN(" << m_syn << ") ";
    if(m_hasAck)
        std::cout << "ACK(" << m_ack << ") ";
    if(m_hasFin)
        std::cout << "FIN ";
    std::cout << "DLEN(" << m_data.size() << ")" << std::endl;
}

const std::string& Packet::getFrom() const
{
    return m_from;
}

const std::string& Packet::getTo() const
{
    return m_to;
}

const std::string& Packet::getData() const
{
    return m_data;
}

uint32_t Packet::getSyn() const
{
    return m_syn;
}

uint32_t Packet::getAck() const
{
    return m_ack;
}

bool Packet::hasSyn() const
{
    return m_hasSyn;
}

bool Packet::hasAck() const
{
    return m_hasAck;
}

bool Packet::hasFin() const
{
    return m_hasFin;
}
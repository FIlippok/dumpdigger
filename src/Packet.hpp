#ifndef __PACKET_HPP__
#define __PACKET_HPP__

#include <string>

class Packet
{
public:
    Packet(const unsigned char* packetData, size_t length);

    const std::string& getFrom() const;
    const std::string& getTo() const;
    const std::string& getData() const;
    uint32_t getSyn() const;
    uint32_t getAck() const;
    bool hasSyn() const;
    bool hasAck() const;
    bool hasFin() const;

    void printInfo() const;

private:
    std::string m_from;
    std::string m_to;
    std::string m_data;
    uint32_t m_syn;
    uint32_t m_ack;
    bool m_hasSyn;
    bool m_hasAck;
    bool m_hasFin;
};

#endif
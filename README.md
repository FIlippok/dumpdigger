DumpDigger
==========

DB Schema:
----------
```
create table captured_requests(
    id bigserial PRIMARY KEY,
    path text not null,
    request text not null,
    response text not null
);
```

Requirements:
-------------
- libpcap
- libpq

Usage:
------
```dumpdigger <db connection string> <pcap file>```
